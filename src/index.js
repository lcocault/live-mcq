/*
 * Copyright Laurent COCAULT 2019
 */
import React from 'react' // eslint-disable-line no-unused-vars
import ReactDOM from 'react-dom'
import MainView from './view/MainView' // eslint-disable-line no-unused-vars

ReactDOM.render(<MainView />, document.getElementById('root'))
