/*
 * Copyright Laurent COCAULT 2019
 */
import React from 'react' // eslint-disable-line no-unused-vars
import renderer from 'react-test-renderer'

import { runFakeUserService } from '../server/FakeUserService'
import MainView from './MainView' // eslint-disable-line no-unused-vars

test('Main view rendering', () => {
  const view = renderer.create(<MainView/>)
  let tree = view.toJSON()
  expect(tree).toMatchSnapshot()
})

test('Successful login management', done => {
  // Install the service
  const server = runFakeUserService()

  // Create the view
  const view = renderer.create(<MainView/>)
  const loginInput = view.root.findByProps({id: 'login'})
  const passwordInput = view.root.findByProps({id: 'password'})

  // Change credentials with valid values
  loginInput.props.onChange({ target: { value: 'john' } })
  passwordInput.props.onChange({ target: { value: 'doe' } })
  expect(view.toJSON()).toMatchSnapshot()

  // Authenticate
  const connectButton = view.root.findByType('button')
  connectButton.props.onClick()

  // Wait until the connection is done
  setTimeout(() => {
    expect(view.toJSON()).toMatchSnapshot()
    server.close()
    done()
  }, 2000)
})

test('Failed login management', done => {
  // Install the service
  const server = runFakeUserService()

  // Create the view
  const view = renderer.create(<MainView/>)
  const loginInput = view.root.findByProps({id: 'login'})
  const passwordInput = view.root.findByProps({id: 'password'})

  // Change credentials with invalid values
  loginInput.props.onChange({ target: { value: 'bill' } })
  passwordInput.props.onChange({ target: { value: 'doe' } })
  expect(view.toJSON()).toMatchSnapshot()

  // Authenticate
  const connectButton = view.root.findByType('button')
  connectButton.props.onClick()

  // Wait until the connection is done
  setTimeout(() => {
    expect(view.toJSON()).toMatchSnapshot()
    server.close()
    done()
  }, 2000)
})
