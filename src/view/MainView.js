/*
 * Copyright Laurent COCAULT 2019
 */
import React from 'react'

import './theme.css'

import { UserServiceConsumer } from '../service/UserServiceConsumer'

import GameView from './GameView' // eslint-disable-line no-unused-vars

/**
 * Render the top level view.
 */
class MainView extends React.Component {
  /**
   * Constructor of the view
   * @param {Object[]} props Properties of the view
   */
  constructor (props) {
    super(props)

    // Create the client proxy
    this.userService = new UserServiceConsumer()

    // Initialize the state
    this.state = { login: '', password: '', session: null, message: null }

    // Bind listeners
    this.connect = this.connect.bind(this)
    this.changeLogin = this.changeLogin.bind(this)
    this.changePassword = this.changePassword.bind(this)
  }

  /**
   * Try to establish a connection.
   * @return {void}
   */
  connect () {
    // Try to authenticate with current credentials
    this.userService.login(
      this.state.login,
      this.state.password,
      (session) => this.setSession(session),
      (error) => this.setMessage(error))
  }

  /**
   * Change the value of the login.
   * @param {Event} evt Login change event
   * @return {void}
   */
  changeLogin (evt) {
    this.setState({
      login: evt.target.value
    })
  }

  /**
   * Change the value of the password.
   * @param {Event} evt Password change event
   * @return {void}
   */
  changePassword (evt) {
    this.setState({
      password: evt.target.value
    })
  }

  /**
   * Change the value of the password.
   * @param {Session} currentSession Session of the user
   * @return {void}
   */
  setSession (currentSession) {
    this.setState({
      session: currentSession
    })
  }

  /**
   * Change the value of the message.
   * @param {String} currentMessage Message to display
   * @return {void}
   */
  setMessage (currentMessage) {
    this.setState({
      message: currentMessage
    })
  }

  /**
   * Render the view.
   */
  render () {
    // Get the state attributes
    let session = this.state.session
    let login = this.state.login
    let password = this.state.password
    let message = this.state.message

    if (session === null || session === undefined) {
      // Undefined session. Prompt for a new one.
      return <div class="authentication-form">
        <h1>Veuillez vous identifier</h1>
        <div class="alert alert-danger" role="alert" hidden={message === null}>{message}</div>
        <div>
          <input
            type="text"
            id="login"
            value={login}
            placeholder="Login"
            onChange={this.changeLogin}
            class="form-control"/>
        </div>
        <div>
          <input
            type="password"
            id="password"
            value={password}
            placeholder="Mot de passe"
            onChange={this.changePassword}
            class="form-control"/>
        </div>
        <div>
          <button
            type="button"
            onClick={this.connect}
            class="btn btn-primary">
            Connexion
          </button>
        </div>
      </div>
    } else {
      // Session known, render the game
      return <GameView session={session} init={true} />
    }
  }
}

export default MainView
