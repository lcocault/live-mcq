/*
 * Copyright Laurent COCAULT 2019
 */
import React from 'react' // eslint-disable-line no-unused-vars
import renderer from 'react-test-renderer'

import { Session } from '../business/Session'
import { Game } from '../business/Game'
import { runFakeGameService } from '../server/FakeGameService'
import GameView from './GameView' // eslint-disable-line no-unused-vars

test('Game view rendering', () => {
  const session = new Session('john', 'myToken')
  const view = renderer.create(<GameView session={session} init={false}/>)
  let tree = view.toJSON()
  expect(tree).toMatchSnapshot()
})

test('Successful game creation', done => {
  // Install the service
  const server = runFakeGameService(null)

  // Create the view
  const session = new Session('john', 'myToken')
  const view = renderer.create(<GameView session={session} init={false}/>)

  // Create the game
  const newGameButton = view.root.findByType('button')
  newGameButton.props.onClick()

  // Wait until the connection is done
  setTimeout(() => {
    expect(view.toJSON()).toMatchSnapshot()
    server.close()
    done()
  }, 2000)
})

test('Game already exists', done => {
  // Install the service
  const game = new Game()
  game.setOrganizer('misterX')
  const server = runFakeGameService(game)

  // Create the view
  const session = new Session('john', 'myToken')
  const view = renderer.create(<GameView session={session} init={true}/>)

  // Wait until the connection is done
  setTimeout(() => {
    expect(view.toJSON()).toMatchSnapshot()
    server.close()
    done()
  }, 2000)
})

test('Successful game participation', done => {
  // Install the service
  const game = new Game()
  game.setOrganizer('misterX')
  const server = runFakeGameService(game)

  // Create the view
  const session = new Session('john', 'myToken')
  const view = renderer.create(<GameView session={session} init={true}/>)

  // Wait until the connection is done
  setTimeout(() => {
    // Participate to the game
    const participateButton = view.root.findByType('button')
    participateButton.props.onClick()

    // Wait until the connection is done
    setTimeout(() => {
      expect(view.toJSON()).toMatchSnapshot()
      server.close()
      done()
    }, 2000)
  }, 2000)
})

test('Close the list of participants', done => {
  // Install the service
  const game = new Game()
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addParticipant('george')
  const server = runFakeGameService(game)

  // Create the view
  const session = new Session('john', 'myToken')
  const view = renderer.create(<GameView session={session} init={true}/>)
  expect(view.toJSON()).toMatchSnapshot()

  // Wait until the connection is done
  setTimeout(() => {
    // Participate to the game
    const closeButton = view.root.findByType('button')
    closeButton.props.onClick()

    // Wait until the connection is done
    setTimeout(() => {
      expect(view.toJSON()).toMatchSnapshot()
      server.close()
      done()
    }, 2000)
  }, 2000)
})

test('Failed game creation', done => {
  // Install the service
  const game = new Game()
  game.setOrganizer('john')
  const server = runFakeGameService(game)

  // Create the view
  const session = new Session('john', 'myToken')
  const view = renderer.create(<GameView session={session} init={false}/>)

  // Create the game
  const newGameButton = view.root.findByType('button')
  newGameButton.props.onClick()

  // Wait until the connection is done
  setTimeout(() => {
    expect(view.toJSON()).toMatchSnapshot()
    server.close()
    done()
  }, 2000)
})
