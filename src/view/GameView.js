/*
 * Copyright Laurent COCAULT 2019
 */
import React from 'react'

import { Question } from '../business/Question'
import { GameServiceConsumer } from '../service/GameServiceConsumer'

/**
 * Render the Game view.
 */
class GameView extends React.Component {
  /**
   * Constructor of the view
   * @param {Object[]} props Properties of the view
   */
  constructor (props) {
    super(props)

    // Create the client proxy
    this.gameService = new GameServiceConsumer()

    // Initialize the state
    this.init = props.init
    this.state = { game: null, session: props.session, message: null }

    // Bind listeners
    this.answer = this.answer.bind(this)
    this.initialize = this.initialize.bind(this)
    this.nextQuestion = this.nextQuestion.bind(this)
    this.newGame = this.newGame.bind(this)
    this.participate = this.participate.bind(this)
  }

  /**
   * Provide an answer to the current question.
   * @return {void}
   */
  answer() {
    const login = this.state.session.getLogin()
    const answer = document.querySelector('input[name="answer"]:checked').value
    this.gameService.answer(login, answer,
      (game) => this.setGame(game),
      (error) => this.setMessage(error))
  }

  /**
   * Initialize the content of the game.
   * @return {void}
   */
  initialize () {
    // Initialize the game with several questions
    const question1 = new Question('Question 1')
    const question2 = new Question('Question 2')
    question1.addAnswer('Answer 1.1', false)
    question1.addAnswer('Answer 1.2', true)
    question1.addAnswer('Answer 1.3', false)
    question2.addAnswer('Answer 2.1', true)
    question2.addAnswer('Answer 2.2', false)
    this.gameService.initialize(
      [question1, question2],
      (game) => this.setGame(game),
      (error) => this.setMessage(error))
  }

  /**
   * Go to the next question.
   * @return {void}
   */
  nextQuestion () {
    // Close the current question and go to the next one
    this.gameService.nextQuestion(
      (game) => this.setGame(game),
      (error) => this.setMessage(error))
  }

  /**
   * Create a new game.
   * @return {void}
   */
  newGame () {
    // Create a new game in which the current user is mister X
    this.gameService.newGame(
      this.state.session.getLogin(),
      (game) => this.setGame(game),
      (error) => this.setMessage(error))
  }

  /**
   * Participate to an existing game.
   * @return {void}
   */
  participate () {
    // Participate to the game
    this.gameService.participate(
      this.state.session.getLogin(),
      (game) => this.setGame(game),
      (error) => this.setMessage(error))
  }

  /**
   * Manage a new game.
   * @param {Game} newGame Game newly created
   * @return {void}
   */
  setGame (newGame) {
    this.setState({
      game: newGame
    })
  }

  /**
   * Change the value of the message.
   * @param {String} currentMessage Message to display
   * @return {void}
   */
  setMessage (currentMessage) {
    this.setState({
      message: currentMessage
    })
  }

  /**
   * Initialize the component when mounted.
   */
  componentDidMount () {
    // Get the current game
    if (this.init) {
      this.gameService.getCurrentGame(
        (game) => this.setGame(game),
        (error) => this.setMessage(error))
    }
  }

  /**
   * Render the final score of the current user.
   * @param {Integer} score Final score of the user
   */
  renderFinalScore(score) {
    return <div class="alert alert-primary" role="alert">La partie est terminée.
      Vous avez marqué {score} points.</div>
  }

  /**
   * Render the final score for all the users.
   * @param {Game} game Game that just ended
   */
  renderFinalScores(game) {
    return <div class="alert alert-primary" role="alert">
      La partie est terminée. Les scores sont les suivants:
      <ul>
        {
          game.getParticipants().map(function(participant) {
            return <li>{participant.getLogin()} a cumulé {participant.getScore()} points</li>
          })
        }
      </ul>
    </div>
  }

  /**
   * Render a button to create a new game.
   * @param {String} message Message to display in the message zone
   */
  renderNewGame(message) {
    return <div class="game-panel">
      <div class="alert alert-danger" role="alert" hidden={message === null}>{message}</div>
      <div>
        <button
          type="button"
          onClick={this.newGame}
          class="btn btn-primary">
          Nouvelle partie
        </button>
      </div>
    </div>
  }

  /**
   * Render a button to invite the user to join the game.
   * @param {Game} game On-going game
   */
  renderParticipantInvitation(game) {
    return <div>
      <div class="alert alert-primary" role="alert">
        Une partie a été créée par {game.getOrganizer().getLogin()}
      </div>
      <button
        type="button"
        onClick={this.participate}
        class="btn btn-primary">
        Je veux y participer
      </button>
    </div>
  }

  /**
   * Render the list of participants on the organizer display while the game
   * is not locked.
   * @param {Game} game On-going game
   */
  renderParticipantsList(game) {
    return <div>
      <div class="alert alert-primary" role="alert">
        La partie est encore ouverte aux participants
      </div>
      <ul>
        {
          game.getParticipants().map(function(participant) {
            return <li>{participant.getLogin()}</li>
          })
        }
      </ul>
      <button
        type="button"
        onClick={this.initialize}
        class="btn btn-primary">
        Clore la liste des participants
      </button>
    </div>
  }

  /**
   * Render the current question.
   * @param {Game} game On-going game
   */
  renderQuestion(game) {
    const question = game.getCurrentQuestion()
    return <div>
      <div class="alert alert-primary" role="alert">{question.getQuestion()}</div>
      <div class="alert alert-info" role="alert">
        <form>
          {
            question.getAnswers().map(function(answer) {
              return <div><input type="radio" name="answer" value={answer}/> {answer}</div>
            })
          }
        </form>
        <button
          type="button"
          onClick={this.answer}
          class="btn btn-primary">
          Répondre
        </button>
      </div>
    </div>
  }

  /**
   * Render the current question progress status.
   * @param {Game} game On-going game
   */
  renderQuestionProgress(game) {
    const question = game.getCurrentQuestion()
    const stats = this.state.game.getCurrentQuestionStats()
    return <div>
      <div class="alert alert-primary" role="alert">{question.getQuestion()}</div>
      <div class="alert alert-info" role="alert">
        {
          stats.map(function(answer) {
            return <div>{answer.answer} = {answer.count}</div>
          })
        }
        <button
          type="button"
          onClick={this.nextQuestion}
          class="btn btn-primary">
          Question suivante
        </button>
      </div>
    </div>
  }

  /**
   * Render the score of the last answer provided.
   * @param {Integer} score Score to display
   */
  renderScore(score) {
    return <div class="alert alert-primary" role="alert">Vous marquez {score} point</div>
  }

  /**
   * Render a message to invite the participant to wait.
   */
  renderWaitingForGameToStart() {
    return <div class="alert alert-primary" role="alert">
      La partie va bientôt démarrer
    </div>
  }

  /**
   * Render the view.
   */
  render () {
    // Get the state of the view
    const game = this.state.game
    const login = this.state.session.getLogin()

    if (game === null) {
      // Undefined game rendering
      return this.renderNewGame()
    } else {
      // The game is defined, get the current user
      let currentUser = game.getUser(login)
      if (currentUser === null) {
        // The game exists but the player does not participate yet
        return this.renderParticipantInvitation(game)
      } else if (login === game.getOrganizer().getLogin()) {
        // Already participating to the game as the organizer
        if (game.locked) {
          if (game.hasCurrentQuestion()) {
            return this.renderQuestionProgress(game)
          } else {
            return this.renderFinalScores(game)
          }
        } else {
          return this.renderParticipantsList(game)
        }
      } else {
        // Participant view
        if (game.locked) {
          // Already participating to the game as a participant
          const score = game.getCurrentScore(login)
          if (score === null) {
            if (game.hasCurrentQuestion()) {
              // Display a question
              return this.renderQuestion(game)
            } else {
              // End of game, display the final score
              return this.renderFinalScore(game.getFinalScore(login))
            }
          } else {
            // Display an answer
            return this.renderScore(score)
          }
        } else {
          // Waiting for the game to start
          return this.renderWaitingForGameToStart()
        }
      }
    }
  }
}

export default GameView
