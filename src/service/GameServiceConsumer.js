/*
 * Copyright Laurent COCAULT 2019
 */
import { Game } from '../business/Game'
import { Question } from '../business/Question'
import { User } from '../business/User'
import { GameService } from './GameService'

/**
 * Convert a JSON game representation to a game object.
 * @param {JSON} jsonGame JSON representation of a game
 * @return {Game} Game instance
 */
function jsonToGame (jsonGame) {
  const game = Object.assign(new Game(), jsonGame)
  const users = []
  const questions = []
  game.organizer = Object.assign(new User(), game.getOrganizer())

  // Copy participants
  game.participants.forEach((user) => {
    const participant = Object.assign(new User(), user)
    users.push(participant)
  })
  game.participants = users

  // Copy questions
  game.questions.forEach((question) => {
    const quest = new Question(question.question)
    question.answers.forEach(function (answer) {
      quest.addAnswer(answer.text, answer.correct)
    })
    questions.push(quest)
  })
  game.questions = questions

  return game
}

/**
 * Game service proxy to be used on the client side.
 */
class GameServiceConsumer extends GameService {
  /**
   * Propose an answer for a given user
   * @param {String} user Login of the author of the answer
   * @param {String} answer Answer proposed by the user
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  answer (user, answer, onSuccess, onFailure) {
    // Socket to use
    const socket = super.getSocket()

    // Register callbacks
    socket.on('setCurrentGame', function (game) {
      game = jsonToGame(game)
      onSuccess(game)
    })
    socket.on('answerFailed', function (error) {
      onFailure(error)
    })

    // Provide the answer
    socket.emit('answer', user, answer)
  }

  /**
   * Get the current game
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  getCurrentGame (onSuccess, onFailure) {
    // Socket to use
    const socket = super.getSocket()

    // Register callbacks
    socket.on('setCurrentGame', function (game) {
      if (game !== null) {
        game = jsonToGame(game)
      }
      onSuccess(game)
    })
    socket.on('setGameFailed', function (error) {
      onFailure(error)
    })

    // Attempt game retrieval
    socket.emit('getCurrentGame')
  }

  /**
   * Initialize the game
   * @param {Question[]} questions List of questions to initialize the game
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  initialize (questions, onSuccess, onFailure) {
    // Socket to use
    const socket = super.getSocket()

    // Register callbacks
    socket.on('setCurrentGame', function (game) {
      onSuccess(jsonToGame(game))
    })
    socket.on('initializeFailed', function (error) {
      onFailure(error)
    })

    // Attempt initialize
    socket.emit('initialize', questions)
  }

  /**
   * Next question of the game
   * @param {Function} onSuccess Function called to provide the current question
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  nextQuestion (onSuccess, onFailure) {
    // Socket to use
    const socket = super.getSocket()

    // Register callbacks
    socket.on('setCurrentGame', function (game) {
      game = jsonToGame(game)
      onSuccess(game)
    })
    socket.on('nextQuestionFailed', function (error) {
      onFailure(error)
    })

    // Attempt next question
    socket.emit('nextQuestion')
  }

  /**
   * Create a new game
   * @param {String} organizerLogin Login of the organizer for the new game
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  newGame (organizerLogin, onSuccess, onFailure) {
    // Socket to use
    const socket = super.getSocket()

    // Register callbacks
    socket.on('setCurrentGame', function (game) {
      onSuccess(jsonToGame(game))
    })
    socket.on('setGameFailed', function (error) {
      onFailure(error)
    })

    // Attempt game creation
    socket.emit('newGame', organizerLogin)
  }

  /**
   * Create a new game
   * @param {String} userLogin Login of a new user for the new game
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  participate (userLogin, onSuccess, onFailure) {
    // Socket to use
    const socket = super.getSocket()

    // Register callbacks
    socket.on('setCurrentGame', function (game) {
      onSuccess(jsonToGame(game))
    })
    socket.on('participateFailed', function (error) {
      onFailure(error)
    })

    // Attempt participant addition
    socket.emit('participate', userLogin)
  }
}

export { GameServiceConsumer }
