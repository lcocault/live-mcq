/*
 * Copyright Laurent COCAULT 2019
 */
import { GameServiceProvider } from './GameServiceProvider'
import { Question } from '../business/Question'

test('Answer', () => {
  // Perform service test
  const service = new GameServiceProvider()
  const question1 = new Question('Question1?')
  const question2 = new Question('Question2?')
  question1.addAnswer('Answer 1.1', false)
  question1.addAnswer('Answer 1.2', true)
  question2.addAnswer('Answer 2.1', true)
  question2.addAnswer('Answer 2.2', false)
  service.newGame('misterX', () => {}, () => {})
  service.participate('paul', () => {}, () => {})
  service.initialize([question1, question2], () => {}, () => {})
  expect(() => service.answer(
    'paul', 'Answer 1.2',
    (data) => { expect(data.getCurrentScore('paul')).toBe(1) },
    (failure) => { throw new Error('Answer failed') }
  )).not.toThrowError()
})

test('Answer fails', () => {
  // Perform service test
  const service = new GameServiceProvider()
  expect(() => service.answer(
    'paul', 'Answer 1.2',
    (data) => { throw new Error('Game should not exist') },
    (failure) => { expect(failure).toEqual('Cannot answer, no game defined') }
  )).not.toThrowError()
})

test('Get null game', () => {
  // Perform service test
  const service = new GameServiceProvider()
  expect(() => service.getCurrentGame(
    (data) => { expect(data).toBeNull() },
    (failure) => { throw new Error('Get game failed') }
  )).not.toThrowError()
})

test('Create new game', () => {
  // Perform service test
  const service = new GameServiceProvider()
  expect(() => service.newGame(
    'john',
    (data) => { expect(data).not.toBeNull() },
    (failure) => { throw new Error('New game failed') }
  )).not.toThrowError()
})

test('Participate', () => {
  // Perform service test
  const service = new GameServiceProvider()
  service.newGame('misterX', () => {}, () => {})
  expect(() => service.participate(
    'john',
    (data) => { expect(data).not.toBeNull() },
    (failure) => { throw new Error('Participate failed') }
  )).not.toThrowError()
})

test('Participate fails', () => {
  // Perform service test
  const service = new GameServiceProvider()
  expect(() => service.participate(
    'john',
    (data) => { throw new Error('Game should not exist') },
    (failure) => { expect(failure).toEqual('Cannot participate, no game defined') }
  )).not.toThrowError()
})

test('Initialize', () => {
  // Perform service test
  const service = new GameServiceProvider()
  const question1 = new Question('Question1?')
  const question2 = new Question('Question2?')
  service.newGame('misterX', () => {}, () => {})
  expect(() => service.initialize(
    [question1, question2],
    (data) => { expect(data).not.toBeNull() },
    (failure) => { throw new Error('Initialize failed') }
  )).not.toThrowError()
})

test('Initialize fails', () => {
  // Perform service test
  const service = new GameServiceProvider()
  expect(() => service.initialize(
    [],
    (data) => { throw new Error('Game should not exist') },
    (failure) => { expect(failure).toEqual('Cannot initialize, no game defined') }
  )).not.toThrowError()
})

test('Next question', () => {
  // Perform service test
  const service = new GameServiceProvider()
  const question1 = new Question('Question1?')
  const question2 = new Question('Question2?')
  service.newGame('misterX', () => {}, () => {})
  service.initialize([question1, question2], () => {}, () => {})
  expect(() => service.nextQuestion(
    (data) => { expect(data).not.toBeNull() },
    (failure) => { throw new Error('Next question failed') }
  )).not.toThrowError()
})

test('Next question fails', () => {
  // Perform service test
  const service = new GameServiceProvider()
  expect(() => service.nextQuestion(
    (data) => { throw new Error('Game should not exist') },
    (failure) => { expect(failure).toEqual('Cannot get next question, no game defined') }
  )).not.toThrowError()
})
