/*
 * Copyright Laurent COCAULT 2019
 */
import { Game } from '../business/Game'
import { Question } from '../business/Question'
import { runFakeGameService } from '../server/FakeGameService'
import { GameServiceConsumer } from './GameServiceConsumer'

test('Answer', done => {
  // Fake server
  const game = new Game()
  const question1 = new Question('Question1?')
  const question2 = new Question('Question2?')
  question1.addAnswer('Answer 1.1', false)
  question1.addAnswer('Answer 1.2', true)
  question2.addAnswer('Answer 2.1', true)
  question2.addAnswer('Answer 2.2', false)
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addQuestion(question1)
  game.addQuestion(question2)
  game.initialize()
  const server = runFakeGameService(game)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.answer(
    'paul',
    'Answer 1.2',
    (data) => {
      // Check a positive score is returned
      expect(data.getCurrentScore('paul')).toBe(1)
      server.close()
      done()
    },
    (failure) => { throw new Error('Answer failed') }
  )).not.toThrowError()
})

test('Answer failure', done => {
  // Fake server
  const server = runFakeGameService(null)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.answer(
    'paul',
    'Answer 1.2',
    (data) => { throw new Error('Should be impossible to answer') },
    (failure) => {
      server.close()
      done()
    }
  )).not.toThrowError()
})

test('Get current game', done => {
  // Fake server
  const game = new Game()
  const question1 = new Question('Question 1?')
  const question2 = new Question('Question 2?')
  game.setOrganizer('john')
  question1.addAnswer('Answer 1.1', false)
  question1.addAnswer('Answer 1.2', true)
  question2.addAnswer('Answer 2.1', true)
  question2.addAnswer('Answer 2.2', false)
  game.addQuestion(question1)
  game.addQuestion(question2)
  const server = runFakeGameService(game)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.getCurrentGame(
    (data) => {
      // Check the game is initially set
      expect(data).not.toBeNull()
      server.close()
      done()
    },
    (failure) => { throw new Error('Get current game failed') }
  )).not.toThrowError()
})

test('Get current game failure', done => {
  // Fake server
  const server = runFakeGameService(null)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.getCurrentGame(
    (data) => {
      // Check the game is not set
      expect(data).toBeNull()
      server.close()
      done()
    },
    (failure) => { throw new Error('Get current game failed') }
  )).not.toThrowError()
})

test('Get current game failure', done => {
  // Fake server
  const server = runFakeGameService(undefined)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.getCurrentGame(
    (data) => { throw new Error('Should be impossible to get an undefined game') },
    (failure) => {
      server.close()
      done()
    }
  )).not.toThrowError()
})

test('Next question', done => {
  // Fake server
  const game = new Game()
  const question1 = new Question('Question 1?')
  const question2 = new Question('Question 2?')
  game.setOrganizer('john')
  game.addQuestion(question1)
  game.addQuestion(question2)
  game.initialize()
  const server = runFakeGameService(game)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.nextQuestion(
    (data) => {
      // Check the game is initially set
      expect(data).not.toBeNull()
      server.close()
      done()
    },
    (failure) => { throw new Error('Next question failed') }
  )).not.toThrowError()
})

test('Next question failure', done => {
  // Fake server
  const server = runFakeGameService(null)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.nextQuestion(
    (data) => { throw new Error('Should be impossible to go to the next question') },
    (failure) => {
      server.close()
      done()
    }
  )).not.toThrowError()
})

test('Initialize', done => {
  // Fake server
  const game = new Game()
  const question1 = new Question('Question1?')
  const question2 = new Question('Question2?')
  game.setOrganizer('john')
  const server = runFakeGameService(game)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.initialize(
    [question1, question2],
    (data) => {
      // Check the game is initially set
      expect(data).not.toBeNull()
      server.close()
      done()
    },
    (failure) => { throw new Error('Initialize failed') }
  )).not.toThrowError()
})

test('Initialize failure', done => {
  // Fake server
  const server = runFakeGameService(null)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.initialize(
    [],
    (data) => { throw new Error('Should be impossible to go to the next question') },
    (failure) => {
      server.close()
      done()
    }
  )).not.toThrowError()
})

test('Create new game', done => {
  // Fake server
  const server = runFakeGameService(null)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.newGame(
    'john',
    (data) => {
      // Check the game is not null
      expect(data).not.toBeNull()
      server.close()
      done()
    },
    (failure) => { throw new Error('New game failed') }
  )).not.toThrowError()
})

test('Create new game failed', done => {
  // Fake server
  const game = new Game()
  game.setOrganizer('john')
  const server = runFakeGameService(game)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.newGame(
    'john',
    (data) => { throw new Error('Should be impossible to overwrite a game') },
    (failure) => {
      server.close()
      done()
    }
  )).not.toThrowError()
})

test('Participate', done => {
  // Fake server
  const game = new Game()
  game.setOrganizer('misterX')
  const server = runFakeGameService(game)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.participate(
    'john',
    (data) => {
      // Check the game is not null
      expect(data).not.toBeNull()
      server.close()
      done()
    },
    (failure) => { throw new Error('Participate failed') }
  )).not.toThrowError()
})

test('Participate failed', done => {
  // Fake server
  const server = runFakeGameService(null)

  // Tested service
  const service = new GameServiceConsumer()

  // Perform service test
  expect(() => service.participate(
    'john',
    (data) => { throw new Error('Should be impossible to participate when the game does not exist') },
    (failure) => {
      server.close()
      done()
    }
  )).not.toThrowError()
})
