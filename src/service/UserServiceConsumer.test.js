/*
 * Copyright Laurent COCAULT 2019
 */
import { runFakeUserService } from '../server/FakeUserService'
import { UserServiceConsumer } from './UserServiceConsumer'

// Setup and dispose the test server
let server = null
beforeEach(() => {
  server = runFakeUserService()
})
afterEach(() => {
  server.close()
})

test('Successful login', done => {
  // Tested service
  const service = new UserServiceConsumer()

  // Perform service test
  expect(() => service.login(
    'john',
    'doe',
    (data) => {
      // Check the session received
      expect(data.login).toEqual('john')
      done()
    },
    (failure) => { throw new Error('Login failed') }
  )).not.toThrowError()
})

test('Unauthorized login', done => {
  // Tested service
  const service = new UserServiceConsumer()

  // Perform service test
  expect(() => service.login(
    'bill',
    'doe',
    (data) => { throw new Error('Login should fail') },
    (failure) => {
      // Check the message received
      expect(failure).toEqual('Invalid credentials')
      done()
    }
  )).not.toThrowError()
})
