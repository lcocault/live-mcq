/*
 * Copyright Laurent COCAULT 2019
 */
import { Session } from '../business/Session'
import { UserService } from './UserService'

/**
 * User service proxy to be used on the client side.
 */
class UserServiceConsumer extends UserService {
  /**
   * Authenticate a user provided that credential are valid
   * @param {String} userLogin Login provided by the user
   * @param {String} userPassword Password provided by the user
   * @param {Function} onSuccess Function called on success with the session
   * @param {Function} onFailure Function called when credentials are invalid
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  login (userLogin, userPassword, onSuccess, onFailure) {
    // Socket to use
    const socket = super.getSocket()

    // Register on success callback
    socket.on('authorized', function (session) {
      // The session received only contains data: it must be recreated on the
      // client side to benefit from functions
      onSuccess(new Session(session.login, session.token))
    })

    // Register on failure callback
    socket.on('unauthorized', function (failure) {
      onFailure(failure)
    })

    // Attempt login
    socket.emit('login', {login: userLogin, password: userPassword})
  }
}

export { UserServiceConsumer }
