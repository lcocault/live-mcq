/*
 * Copyright Laurent COCAULT 2019
 */
import { GameService } from './GameService'

test('Undefined service', () => {
  const service = new GameService()
  expect(() => service.answer()).toThrowError()
  expect(() => service.getCurrentGame()).toThrowError()
  expect(() => service.initialize()).toThrowError()
  expect(() => service.nextQuestion()).toThrowError()
  expect(() => service.newGame()).toThrowError()
  expect(() => service.participate()).toThrowError()
})
