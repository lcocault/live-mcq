/*
 * Copyright Laurent COCAULT 2010
 */
import { Session } from '../business/Session'
import { UserServiceProvider } from './UserServiceProvider'

test('New user', () => {
  // Perform service test
  const service = new UserServiceProvider()
  expect(() => service.login(
    'john',
    'doe',
    (data) => { expect(data).not.toBeNull() },
    (failure) => { throw new Error('Login failed') }
  )).not.toThrowError()
})

test('Existing session', () => {
  // Create the session
  const service = new UserServiceProvider()
  service.sessions.set('john', new Session('john', 'doe'))
  // Perform service test
  expect(() => service.login(
    'john',
    'doe',
    (data) => { throw new Error('Session already open') },
    (failure) => { expect(failure).toEqual('Session already open') }
  )).not.toThrowError()
})

test('New session with valid credentials', () => {
  // Create the session
  const service = new UserServiceProvider()
  service.passwords.set('john', 'doe')
  // Perform service test
  expect(() => service.login(
    'john',
    'doe',
    (data) => { expect(data).not.toBeNull() },
    (failure) => { throw new Error('Login failed') }
  )).not.toThrowError()
})

test('New session with invalid credentials', () => {
  // Create the session
  const service = new UserServiceProvider()
  service.passwords.set('john', 'wayne')
  // Perform service test
  expect(() => service.login(
    'john',
    'doe',
    (data) => { throw new Error('Invalid credentials') },
    (failure) => { expect(failure).toEqual('Invalid credentials') }
  )).not.toThrowError()
})
