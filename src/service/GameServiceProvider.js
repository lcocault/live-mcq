/*
 * Copyright Laurent COCAULT 2019
 */
import { GameService } from './GameService'
import { Game } from '../business/Game'
import { Question } from '../business/Question'

/**
 * Game service provider to be used on the server side.
 */
class GameServiceProvider extends GameService {
  /**
   * Game service provider initialization.
   */
  constructor () {
    super()
    this.game = null
  }

  /**
   * Answer to a question
   * @param {String} user Author of the question
   * @param {String} answer Answer given by the user
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  answer (user, answer, onSuccess, onFailure) {
    if (this.game === null) {
      // If the game does not exist, the answer cannot be given.
      onFailure('Cannot answer, no game defined')
    } else {
      this.game.addAnswer(user, answer)
      onSuccess(this.game)
    }
  }

  /**
   * Get the current game
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  getCurrentGame (onSuccess, onFailure) {
    onSuccess(this.game)
  }

  /**
   * Initialize
   * @param {Question[]} questions List of questions to initialize the game
   * @param {Function} onSuccess Function called to provide the game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  initialize (questions, onSuccess, onFailure) {
    if (this.game === null) {
      // If the game does not exist, the next question cannot be retrieved.
      onFailure('Cannot initialize, no game defined')
    } else {
      questions.forEach((question) => {
        const quest = new Question(question.question)
        question.answers.forEach(function (answer) {
          quest.addAnswer(answer.text, answer.correct)
        })
        this.game.addQuestion(quest)
      })
      this.game.initialize()
      onSuccess(this.game)
    }
  }

  /**
   * Next question
   * @param {Function} onSuccess Function called to provide the next question
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  nextQuestion (onSuccess, onFailure) {
    if (this.game === null) {
      // If the game does not exist, the next question cannot be retrieved.
      onFailure('Cannot get next question, no game defined')
    } else {
      this.game.nextQuestion()
      onSuccess(this.game)
    }
  }

  /**
   * Create a new game
   * @param {String} organizerLogin Login of the organizer for the new game
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  newGame (organizerLogin, onSuccess, onFailure) {
    // Create the game and provide it through the onSuccess method. There is
    // currently no reason for the creation of the game to fail; this is why
    // the onFailure method is not used.
    this.game = new Game()
    this.game.setOrganizer(organizerLogin)
    onSuccess(this.game)
  }

  /**
   * Participate to the current game
   * @param {String} userLogin Login of the user that joins the game
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  participate (userLogin, onSuccess, onFailure) {
    if (this.game === null) {
      // If the game does not exist, the participation is impossible.
      onFailure('Cannot participate, no game defined')
    } else {
      // If the game does exist, the participant is added
      this.game.addParticipant(userLogin)
      onSuccess(this.game)
    }
  }
}

export { GameServiceProvider }
