/*
 * Copyright Laurent COCAULT 2019
 */
import { UserService } from './UserService'

test('Undefined service', () => {
  const service = new UserService()
  expect(() => service.login('john', 'doe')).toThrowError()
})
