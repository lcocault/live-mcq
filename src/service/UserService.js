/*
 * Copyright Laurent COCAULT 2019
 */
import {Service} from './Service'

/**
 * User service.
 */
class UserService extends Service {
  /**
   * Authenticate a user provided that credential are valid
   * @param {String} login Login provided by the user
   * @param {String} password Password provided by the user
   * @param {Function} onSuccess Function called on success with the session
   * @param {Function} onFailure Function called when credentials are invalid
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  login (login, password, onSuccess, onFailure) {
    throw new Error('Login function is not implemented')
  }
}

export { UserService }
