/*
 * Copyright Laurent COCAULT 2019
 */
import io from 'socket.io-client'

class Service {
  /**
   * Get the address of the service.
   * @return {String} Service address
   */
  getAddress() {
    return process.env.IP
  }
  /**
   * Get the port of the service.
   * @return {Number} Service port
   */
  getPort () {
    return process.env.PORT
  }
  /**
   * Get a client socket to reach the service.
   * @return {Socket} Client socket
   */
  getSocket () {
    if (this.socket === undefined) {
      // Create the socket when it does exist
      this.socket = io.connect('http://'+this.getAddress()+':'+this.getPort())
    }
    return this.socket
  }
}

export { Service }
