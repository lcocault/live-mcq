/*
 * Copyright Laurent COCAULT 2019
 */
import { Session } from '../business/Session'
import { UserService } from './UserService'

/**
 * User service provider to be used on the server side.
 */
class UserServiceProvider extends UserService {
  /**
   * User service provider initialization.
   */
  constructor () {
    super()
    this.passwords = new Map()
    this.sessions = new Map()
  }

  /**
   * Authenticate a user provided that credential are valid
   * @param {String} login Login provided by the user
   * @param {String} password Password provided by the user
   * @param {Function} onSuccess Function called on success with the session
   * @param {Function} onFailure Function called when credentials are invalid
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  login (login, password, onSuccess, onFailure) {
    // First check if a session is already open
    let session = this.sessions.get(login)
    if (session === undefined) {
      // Session does not exist, check if the user exists and has the same
      // password as the one provided
      const refPassword = this.passwords.get(login)
      if (refPassword === undefined) {
        // Unknown user, create it and return a new session
        this.passwords.set(login, password)
        session = new Session(login)
        this.sessions.set(session)
        onSuccess(session)
      } else if (refPassword === password) {
        // Password is correct
        session = new Session(login)
        this.sessions.set(session)
        onSuccess(session)
      } else {
        // Password is invalid
        onFailure('Invalid credentials')
      }
    } else {
      // Failure: session is already open
      onFailure('Session already open')
    }
  }
}

export { UserServiceProvider }
