/*
 * Copyright Laurent COCAULT 2019
 */
import {Service} from './Service'

/**
 * Game service.
 */
class GameService extends Service {
  /**
   * Provide an answer to the current question for a given user.
   * @param {String} user Login of the author of the answer
   * @param {String} answer Answer proposed by the user
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   */
  answer (user, answer, onSuccess, onFailure) {
    throw new Error('Answer function is not implemented')
  }

  /**
   * Get the current game
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  getCurrentGame (onSuccess, onFailure) {
    throw new Error('Get current game function is not implemented')
  }

  /**
   * Initialize the game
   * @param {Question[]} questions List of questions to initialize the game
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  initialize (questions, onSuccess, onFailure) {
    throw new Error('Initialize function is not implemented')
  }

  /**
   * Next question
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  nextQuestion (onSuccess, onFailure) {
    throw new Error('Next question function is not implemented')
  }

  /**
   * Create a new game
   * @param {String} organizerLogin Login of the organizer for the new game
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  newGame (organizerLogin, onSuccess, onFailure) {
    throw new Error('New game function is not implemented')
  }

  /**
   * Participate to the current game
   * @param {String} userLogin Login of the user that joins the game
   * @param {Function} onSuccess Function called to provide the current game
   * @param {Function} onFailure Function called when an error occurs
   * @return {void}
   * @throws {Error} Cannot submit the request
   */
  participate (userLogin, onSuccess, onFailure) {
    throw new Error('Participate function is not implemented')
  }
}

export { GameService }
