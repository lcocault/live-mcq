/*
 * Copyright Laurent COCAULT 2019
 */
import { Session } from './Session'

test('Session initialization without token', () => {
  const session = new Session('john')
  expect(session.getLogin()).toBe('john')
  expect(session.token).toContain('token-')
})

test('Session initialization with token', () => {
  const session = new Session('john', 'myToken')
  expect(session.getLogin()).toBe('john')
  expect(session.token).toBe('myToken')
})
