/*
 * Copyright Laurent COCAULT 2019
 */
import { Game } from './Game'
import { Question } from './Question'

test('Game initialization', () => {
  const game = new Game()
  game.setOrganizer('john')
  expect(game.getOrganizer().getLogin()).toBe('john')
  expect(game.getParticipants().length).toBe(0)
  expect(() => game.getCurrentQuestion()).toThrowError()
})

test('Adding a user to a game', () => {
  const game = new Game()
  game.setOrganizer('john')
  expect(() => game.addParticipant('paul')).not.toThrowError()
})

test('Get a user with an unknown login', () => {
  const game = new Game()
  game.setOrganizer('john')
  game.addParticipant('george')
  expect(game.getParticipants().length).toBe(1)
  expect(game.getUser('paul')).toBeNull()
})

test('Get a user to its login', () => {
  const game = new Game()
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addParticipant('george')
  expect(game.getParticipants().length).toBe(2)
  expect(game.getUser('paul')).not.toBeNull()
})

test('Get a participant with an unknown login', () => {
  const game = new Game()
  game.setOrganizer('john')
  game.addParticipant('george')
  expect(game.getUser('paul')).toBeNull()
})

test('Get a User by its login', () => {
  const game = new Game()
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addParticipant('george')
  expect(game.getUser('paul')).not.toBeNull()
})

test('Get the organizer by its login', () => {
  const game = new Game()
  game.setOrganizer('john')
  game.addParticipant('paul')
  expect(game.getUser('john')).not.toBeNull()
})

test('Adding a user to a locked game', () => {
  const game = new Game()
  game.setOrganizer('john')
  game.initialize()
  expect(() => game.addParticipant('paul')).toThrowError()
})

test('Adding a question to a game', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  game.addQuestion(question)
  expect(() => game.addQuestion(question)).not.toThrowError()
  expect(() => game.getCurrentQuestion()).toThrowError()
})

test('Next question for an unlocked game', () => {
  const game = new Game()
  const question1 = new Question('Question1')
  const question2 = new Question('Question2')
  game.addQuestion(question1)
  game.addQuestion(question2)
  expect(() => game.nextQuestion()).toThrowError()
})

test('Getting the final score for an unlocked game', () => {
  const game = new Game()
  const question1 = new Question('Question1')
  const question2 = new Question('Question2')
  game.addQuestion(question1)
  game.addQuestion(question2)
  expect(() => game.getFinalScore('paul')).toThrowError()
})

test('Is there a current question for an unlocked game', () => {
  const game = new Game()
  const question1 = new Question('Question1')
  const question2 = new Question('Question2')
  game.addQuestion(question1)
  game.addQuestion(question2)
  expect(() => game.hasCurrentQuestion()).toThrowError()
})

test('Adding a question to a locked game', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  game.initialize()
  expect(() => game.addQuestion(question)).toThrowError()
})

test('Proposing an answer to a locked game', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  question.addAnswer('To be', false)
  question.addAnswer('Not to be', true)
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addQuestion(question)
  game.initialize()
  expect(() => game.addAnswer('paul', 'Not to be')).not.toThrowError()
})

test('Getting the score of a valid answer', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  question.addAnswer('To be', false)
  question.addAnswer('Not to be', true)
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addQuestion(question)
  game.initialize()
  game.addAnswer('paul', 'Not to be')
  expect(game.getCurrentScore('paul')).toBe(1)
})

test('Getting the score of a participant', () => {
  const game = new Game()
  const question1 = new Question('To be or not to be?')
  const question2 = new Question('Are you sure?')
  question1.addAnswer('To be', false)
  question1.addAnswer('Not to be', true)
  question2.addAnswer('No', true)
  question2.addAnswer('Yes', false)
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addParticipant('george')
  game.addQuestion(question1)
  game.addQuestion(question2)
  game.initialize()
  game.addAnswer('paul', 'Not to be')
  game.nextQuestion()
  game.addAnswer('paul', 'No')
  game.nextQuestion()
  expect(game.getFinalScore('paul')).toBe(2)
  expect(game.getFinalScore('george')).toBe(0)
})

test('Getting the score of another user', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  question.addAnswer('To be', false)
  question.addAnswer('Not to be', true)
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addQuestion(question)
  game.initialize()
  game.addAnswer('paul', 'Not to be')
  expect(game.getCurrentScore('goerge')).toBeNull()
})

test('Getting the score of an invalid answer', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  question.addAnswer('To be', false)
  question.addAnswer('Not to be', true)
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addQuestion(question)
  game.initialize()
  game.addAnswer('paul', 'To be')
  expect(game.getCurrentScore('paul')).toBe(0)
})

test('Getting the score of a not provided answer', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  question.addAnswer('To be', false)
  question.addAnswer('Not to be', true)
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addQuestion(question)
  game.initialize()
  expect(game.getCurrentScore('paul')).toBeNull()
})

test('Proposing an answer for a non participant', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addQuestion(question)
  game.initialize()
  expect(() => game.addAnswer('george', 'To be')).toThrowError()
})

test('Proposing an answer to an unlocked game', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addQuestion(question)
  expect(() => game.addAnswer('john', 'To be')).toThrowError()
})

test('Getting a score from an unlocked game', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addQuestion(question)
  expect(() => game.getCurrentScore('john')).toThrowError()
})

test('Next question for a locked game', () => {
  const game = new Game()
  const question1 = new Question('Question1')
  const question2 = new Question('Question2')
  game.addQuestion(question1)
  game.addQuestion(question2)
  game.initialize()
  expect(game.getCurrentQuestion()).toBe(question1)
  expect(game.nextQuestion()).toBe(true)
  expect(game.getCurrentQuestion()).toBe(question2)
  expect(game.nextQuestion()).toBe(false)
})

test('Is there a current question for a locked game', () => {
  const game = new Game()
  const question1 = new Question('Question1')
  const question2 = new Question('Question2')
  game.addQuestion(question1)
  game.addQuestion(question2)
  game.initialize()
  expect(game.hasCurrentQuestion()).toBe(true)
  game.nextQuestion()
  expect(game.hasCurrentQuestion()).toBe(true)
  game.nextQuestion()
  expect(game.hasCurrentQuestion()).toBe(false)
})

test('Getting the statistics of a question', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  question.addAnswer('To be', false)
  question.addAnswer('Not to be', true)
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addParticipant('george')
  game.addParticipant('ringo')
  game.addQuestion(question)
  game.initialize()
  game.addAnswer('paul', 'Not to be')
  game.addAnswer('george', 'To be')
  game.addAnswer('ringo', 'Not to be')
  expect(game.getCurrentQuestionStats()[0].answer).toBe('To be')
  expect(game.getCurrentQuestionStats()[0].count).toBe(1)
  expect(game.getCurrentQuestionStats()[1].answer).toBe('Not to be')
  expect(game.getCurrentQuestionStats()[1].count).toBe(2)
})

test('Initializing a game', () => {
  const game = new Game()
  const question = new Question('To be or not to be?')
  game.setOrganizer('john')
  game.addParticipant('paul')
  game.addParticipant('george')
  game.addQuestion(question)
  expect(() => game.initialize()).not.toThrowError()
  expect(game.getCurrentQuestion()).toBe(question)
})
