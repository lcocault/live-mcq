/*
 * Copyright Laurent COCAULT 2019
 */

/**
 * This class represent a user of the service, including the organizer.
 */
class User {
  /**
   * Constructor of the user.
   * @param {String} login Login of the user
   */
  constructor (login) {
    this.login = login
    this.score = 0
  }

  /**
   * Add points to the current score of the user.
   * @param {Integer} points Number of points to add
   * @return {void}
   */
  addScore(points) {
    this.score += points
  }

  /**
   * Get the login of the user.
   * @return {String} The user login
   */
  getLogin () {
    return this.login
  }

  /**
   * Get the score of the user.
   * @return {Integer} The user score
   */
  getScore () {
    return this.score
  }
}

export { User }
