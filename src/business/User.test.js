/*
 * Copyright Laurent COCAULT 2019
 */
import { User } from './User'

test('User initialization', () => {
  const user = new User('user')
  expect(user.getLogin()).toBe('user')
  expect(user.getScore()).toBe(0)
})

test('Adding points to the user score', () => {
  const user = new User('user')
  user.addScore(3)
  expect(user.getScore()).toBe(3)
  user.addScore(2)
  expect(user.getScore()).toBe(5)
})
