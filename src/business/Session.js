/*
 * Copyright Laurent COCAULT 2019
 */
class Session {
  /**
   * Constructor of the session
   * @param {String} login Login of the authenticated user
   * @param {String} token Token of the session
   */
  constructor (login, token) {
    this.login = login
    if (token === undefined) {
      this.token = 'token-' + Math.floor(Math.random() * 1000000)
    } else {
      this.token = token
    }
  }

  /**
   * Get the login of the authenticated user
   * @return {String} The user login
   */
  getLogin () {
    return this.login
  }
}

export { Session }
