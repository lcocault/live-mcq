/*
 * Copyright Laurent COCAULT 2019
 */

/**
 * The "Question" class represents a question, the different options and the
 * appropriate answer(s).
 */
class Question {
  /**
   * Constructor of the question.
   */
  constructor (question) {
    // Question to ask
    this.question = question
    // Answers
    this.answers = []
  }

  /**
   * Add an answer to the question.
   * @param {String} answer The answer to add
   * @param {Boolean} correct A flag to tell if the answer is correct or not
   * @return {void}
   * @throws {Error} The user cannot be added when the game is locked
   */
  addAnswer (answer, correct) {
    this.answers.push({text: answer, correct: correct})
  }

  /**
   * Get the answers of the question.
   * @return {String[]} Array containing the answers to the question
   */
  getAnswers () {
    const answers = []
    this.answers.forEach((answer) => answers.push(answer.text))
    return answers
  }

  /**
   * Get the text of the question.
   * @return {String} Question to ask
   */
  getQuestion () {
    return this.question
  }

  /**
   * Get the score of the given answer.
   * @param {String} givenAnswer Answer to evaluate
   * @return {Integer} Score of the answer, 0 when the answer is not  valid one
   */
  getScore(givenAnswer) {
    let score = 0
    this.answers.forEach((answer) => {
      if (answer.correct && givenAnswer === answer.text) {
        score = 1
      }
    })
    return score
  }

  /**
   * Get the optional resource associated to the question.
   * @return {String} Question optional resource
   */
  getResource () {
    return this.resource
  }

  /**
   * Set the optional resource associated to the question.
   * @param {String} resource Question optional resource
   */
  setResource (resource) {
    this.resource = resource
  }
}

export { Question }
