/*
 * Copyright Laurent COCAULT 2019
 */
import { Question } from './Question'

test('Question initialization', () => {
  const question = new Question('To be or not to be?')
  expect(question.getQuestion()).toBe('To be or not to be?')
  expect(question.getAnswers().length).toBe(0)
  expect(question.getResource()).toBeUndefined()
})

test('Two answers', () => {
  const question = new Question('To be or not to be?')
  question.addAnswer('To be', false)
  question.addAnswer('Not to be', true)
  const answers = question.getAnswers()
  expect(answers.length).toBe(2)
  expect(answers[0]).toBe('To be')
  expect(answers[1]).toBe('Not to be')
})

test('Question resource management', () => {
  const question = new Question('To be or not to be?')
  question.setResource('My resource')
  expect(question.getResource()).toBe('My resource')
})

test('Invalid answer score', () => {
  const question = new Question('To be or not to be?')
  question.addAnswer('To be', false)
  question.addAnswer('Not to be', true)
  expect(0).toBe(question.getScore('To be'))
})

test('Unknown answer score', () => {
  const question = new Question('To be or not to be?')
  question.addAnswer('To be', false)
  question.addAnswer('Not to be', true)
  expect(0).toBe(question.getScore('Dont know'))
})

test('Valid answer score', () => {
  const question = new Question('To be or not to be?')
  question.addAnswer('To be', false)
  question.addAnswer('Not to be', true)
  expect(1).toBe(question.getScore('Not to be'))
})
