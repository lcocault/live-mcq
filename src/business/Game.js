/*
 * Copyright Laurent COCAULT 2019
 */
import { User } from './User'

/**
 * The "Game" class in the main entry point for the game logic. It manages the
 * users and the questions.
 */
class Game {
  /**
   * Constructor of the game.
   */
  constructor () {
    // Game is not initially locked
    this.locked = false
    // initially no user
    this.participants = []
    // initially no question
    this.questions = []
  }

  /**
   * Add an answer to the current question for a given user identified by its
   * login.
   * @param {String} user The login of the author of the answer
   * @param {String} answer The answer given to the current question
   * @return {void}
   * @throws {Error} No answer can be proposed until the game is locked
   */
  addAnswer(user, answer) {
    if (!this.locked) {
      throw new Error('Cannot propose an answer until the game is locked')
    }
    const participant = this.getParticipant(user)
    if (participant === null) {
      throw new Error('User does not participate')
    }
    const score = this.getCurrentQuestion().getScore(answer)
    participant.addScore(score)
    this.answers.push({author: user, answer: answer, score: score})
  }

  /**
   * Add a participant to the game.
   * @param {String} user The login of the user to add
   * @return {void}
   * @throws {Error} The user cannot be added when the game is locked
   */
  addParticipant (user) {
    if (this.locked) {
      throw new Error('Cannot add a user when the game is locked')
    }
    this.participants.push(new User(user))
  }

  /**
   * Add a question to the game.
   * @param {Question} question The question to add
   * @return {void}
   * @throws {Error} The question cannot be added when the game is locked
   */
  addQuestion (question) {
    if (this.locked) {
      throw new Error('Cannot add a question when the game is locked')
    }
    this.questions.push(question)
  }

  /**
   * Initialize the game. It is no longer possible to add participant after
   * this method has been called.
   */
  initialize () {
    // Lock the game
    this.locked = true
    // Set the pointer of the current question
    this.currentQuestion = 0
    // Initialize the list of given answers
    this.answers = []
  }

  /**
   * Get the current question to ask.
   * @return {Question} The next question to ask
   * @throws {Error} The game is not initialized or there is no more question
   * to ask
   */
  getCurrentQuestion() {
    if (!this.locked || this.currentQuestion >= this.questions.length) {
      throw new Error('No question is available')
    }
    return this.questions[this.currentQuestion]
  }

  /**
   * Get the statistics of the current question.
   * @return {AnswerCount[]} The answers with the number of participants providing them
   * @throws {Error} The game is not initialized or there is no more question
   * to ask
   */
  getCurrentQuestionStats() {
    const question = this.getCurrentQuestion()
    let result = []
    question.getAnswers().forEach((answer) => {
      let count = 0
      this.answers.forEach((givenAnswer) => {
        if (answer === givenAnswer.answer) {
          count++
        }
      })
      result.push({answer: answer, count: count})
    })
    return result
  }

  /**
   * Get the current score for the current question of the provided user.
   * @param {String} user Login of the user for which a score is expected
   * @return {Integer} The score of the user for the current question (null if
   * the user did not provided an answer yet)
   * @throws {Error} The game is not initialized or there is no more question
   * to ask
   */
  getCurrentScore(user) {
    if (!this.locked) {
      throw new Error('No question is available')
    }
    let score = null
    this.answers.forEach((answer) => {
      if (answer.author===user) {
        score = answer.score
      }
    })
    return score
  }

  /**
   * Get the final score for the provided user.
   * @param {String} user Login of the user for which a score is expected
   * @return {Integer} The score of the user
   * @throws {Error} The game is not initialized
   */
  getFinalScore(user) {
    if (!this.locked) {
      throw new Error('No question is available')
    }
    let score = null
    this.participants.forEach((participant) => {
      if (participant.getLogin()===user) {
        score = participant.getScore()
      }
    })
    return score
  }

  /**
   * Get the participants in the game
   * @return {User[]} Array of user playing the role of participants
   */
  getParticipants () {
    return this.participants
  }

  /**
   * Get a user participating to the game by its login.
   * @param {String} login Login of the user to get
   * @return {User} User having the provided login, null if the login
   * does not match any user login
   */
  getParticipant (login) {
    let currentUser = null
    this.participants.forEach((user) => {
      if (login === user.getLogin()) {
        currentUser = user
      }
    })
    return currentUser
  }

  /**
   * Get a user participating to the game by its login.
   * @param {String} login Login of the user to get
   * @return {Player} User having the provided login, null if the login
   * does not match any user login or the organizer one
   */
  getUser (login) {
    if (this.organizer.login === login) {
      // The login provided is the one of mister X
      return this.organizer
    } else {
      // Check if the login is the one of a user
      return this.getParticipant(login)
    }
  }

  /**
   * Get the organizer reference.
   * @return {Player} Organizer
   */
  getOrganizer () {
    return this.organizer
  }

  /**
   * Check if the game still has a question to provide.
   * @return {Boolean} true if a current question is available
   * @throws {Error} The game is not initialized
   */
  hasCurrentQuestion() {
    if (!this.locked) {
      throw new Error('Cannot check question availability until the game is locked')
    }
    return this.currentQuestion < this.questions.length
  }

  /**
   * Go to the next question.
   * @return {Boolean} true if a current question is available
   * @throws {Error} The game is not initialized or there is no more question
   * to ask
   */
  nextQuestion() {
    if (!this.locked) {
      throw new Error('Cannot iterate on questions until the game is locked')
    }
    this.answers = []
    this.currentQuestion++
    return this.currentQuestion < this.questions.length
  }

  /**
   * Set the identity of the organizer.
   * @param {String} organizer Login of the organizer
   * @return {void}
   */
  setOrganizer (organizer) {
    // Organizer identity
    this.organizer = new User(organizer)
  }
}

export { Game }
