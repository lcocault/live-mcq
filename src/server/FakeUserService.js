/*
 * Copyright Laurent COCAULT 2019
 */
import { Session } from '../business/Session'
import { UserServiceConsumer } from '../service/UserServiceConsumer'

export function runFakeUserService () {
  // Tested service
  const service = new UserServiceConsumer()

  // Fake local server
  const server = require('http').createServer()
  const io = require('socket.io')(server)

  // Fake server behaviour
  io.sockets.on('connect', (socket) => {
    socket.on('login', (credentials) => {
      // Manage the login call
      if (credentials.login === 'john') {
        socket.emit('authorized', new Session('john'))
      } else {
        socket.emit('unauthorized', 'Invalid credentials')
      }
    })
  })

  // Start the fake server
  server.listen(service.getPort())

  // And return it
  return server
}
