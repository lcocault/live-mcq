/*
 * Copyright Laurent COCAULT 2019
 */
import { Game } from '../business/Game'
import { GameServiceConsumer } from '../service/GameServiceConsumer'

export function runFakeGameService (initialGame) {
  // Tested service
  const service = new GameServiceConsumer()

  // Fake local server
  const server = require('http').createServer()
  const io = require('socket.io')(server)

  // Fake server behaviour
  io.sockets.on('connect', (socket) => {
    socket.on('answer', (user, answer) => {
      // Manage the call
      if (initialGame === null) {
        io.sockets.emit('answerFailed', 'Game is not defined')
      } else {
        initialGame.addAnswer(user, answer)
        io.sockets.emit('setCurrentGame', initialGame)
      }
    })
    socket.on('getCurrentGame', () => {
      // Manage the call
      if (initialGame === undefined) {
        io.sockets.emit('setGameFailed', 'Undefined game')
      } else {
        io.sockets.emit('setCurrentGame', initialGame)
      }
    })
    socket.on('newGame', (organizerLogin) => {
      // Manage the call
      const game = new Game()
      game.setOrganizer('john')
      if (initialGame === null) {
        io.sockets.emit('setCurrentGame', game)
      } else {
        io.sockets.emit('setGameFailed', 'Already defined game')
      }
    })
    socket.on('participate', (userLogin) => {
      // Manage the call
      if (initialGame === null) {
        io.sockets.emit('participateFailed', 'Game is not defined')
      } else {
        initialGame.addParticipant(userLogin)
        io.sockets.emit('setCurrentGame', initialGame)
      }
    })
    socket.on('initialize', (questions) => {
      // Manage the call
      if (initialGame === null) {
        io.sockets.emit('initializeFailed', 'Game is not defined')
      } else {
        questions.forEach((question) => initialGame.addQuestion(question))
        initialGame.initialize()
        io.sockets.emit('setCurrentGame', initialGame)
      }
    })
    socket.on('nextQuestion', () => {
      // Manage the call
      if (initialGame === null) {
        io.sockets.emit('nextQuestionFailed', 'Game is not defined')
      } else {
        initialGame.nextQuestion()
        io.sockets.emit('setCurrentGame', initialGame)
      }
    })
  })

  // Start the fake server
  server.listen(service.getPort())

  // And return it
  return server
}
