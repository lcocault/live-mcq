/*
 * Copyright Laurent COCAULT 2019
 */
import { GameServiceProvider } from '../service/GameServiceProvider'
import { UserServiceProvider } from '../service/UserServiceProvider'

// Define the server
const port = process.env.PORT
const server = require('http').createServer()
const io = require('socket.io')(server)

// Services to publish
const userService = new UserServiceProvider()
const gameService = new GameServiceProvider()

// Connection management
io.sockets.on('connect', (socket) => {
  console.log('Connected')

  // Login management
  socket.on('login', function (credentials) {
    console.log('Login attempt for ' + credentials.login)
    userService.login(credentials.login, credentials.password,
      (session) => { io.sockets.emit('authorized', session) },
      (failure) => { io.sockets.emit('unauthorized', failure) })
  })

  // Game management
  socket.on('answer', (user, answer) => {
    console.log(user + ' answers ' + answer)
    gameService.answer(user, answer,
      (game) => { io.sockets.emit('setCurrentGame', game) },
      (failure) => { io.sockets.emit('answerFailed', failure) })
  })
  socket.on('getCurrentGame', () => {
    gameService.getCurrentGame(
      (game) => { io.sockets.emit('setCurrentGame', game) },
      (failure) => { io.sockets.emit('setGameFailed', failure) })
  })
  socket.on('initialize', (questions) => {
    console.log('Initialize the game with ' + questions.length + ' questions')
    gameService.initialize(questions,
      (game) => { io.sockets.emit('setCurrentGame', game) },
      (failure) => { io.sockets.emit('initializeFailed', failure) })
  })
  socket.on('nextQuestion', () => {
    console.log('Go to the next question')
    gameService.nextQuestion(
      (game) => { io.sockets.emit('setCurrentGame', game) },
      (failure) => { io.sockets.emit('nextQuestionFailed', failure) })
  })
  socket.on('newGame', (organizerLogin) => {
    console.log('Creating new game for ' + organizerLogin)
    gameService.newGame(organizerLogin,
      (game) => { io.sockets.emit('setCurrentGame', game) },
      (failure) => { io.sockets.emit('setGameFailed', failure) })
  })
  socket.on('participate', (userLogin) => {
    console.log(userLogin + ' is now participating to the game')
    gameService.participate(userLogin,
      (game) => { io.sockets.emit('setCurrentGame', game) },
      (failure) => { io.sockets.emit('participateFailed', failure) })
  })
})

server.listen(port, () => {
  console.log('Listening on port ' + port)
})
