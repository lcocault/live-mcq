# Live MCQ

Live MCQ is a web solution that provides services to submit MCQ (Multiple Choice
Questions) to a group of persons, typically in the frame of a training session.
The organizer of the live MCQ session, for a instance the trainer, is able to
consult the answers in real time and to manage the pace at which the questions
are submitted to the participants, typically the trainees.

## How to use it?

To build the deployable components, clone the source repository and execute the following instruction from the root directory.

1) npm install

After this command has been executed, the node_modules directory should have been populated with the project dependencies and tools.

2) npm run build

This command creates the "dist" directory in which two files are generated.
- bundle.js is the client bundle
- bundle-server.js is the server bundle

Both files may be uploaded to a server able to run npm and node commands. Before deploying these two components, the following environment must be defined on the host:
- IP is the public IP address of the server
- PORT is the port number that the application server may use

The server bundle does not include several runtime dependencies that may have to be installed before running the server. To do so, execute the following commands:

1) npm install babel-register

2) npm install babel-polyfill

3) npm install socket.io-client

4) npm install socket.io

5) start-server.sh

## How does it work?

### System dynamics

The dynamics of the system is summarized in the following sequence diagram:

![General dynamics](uml/phases.png)

The phases of the system dynamics are the following:

1) When users first connect to the system, no game is initially created. Each authenticated user can create the game and thus become the organizer.

2) Once the game is created, other authenticated users can register to participate.

3) Whenever he wants, the organizer can close the subscriptions and make the game enter in the "questions" phase. During this phase, questions are asked to the participants; each question comes with a list of possible answers.

4) When the last question has been closed by the organizer, the game ends and a summary of the results is shown to each participant.

During the questions phase, each user can provide an answer. When the organizer considers enough time has been left to answer, he can move to the next question.

![Questions phase](uml/questions-phase.png)

### Architecture

The architecture of the system is based on a full-stack client/server approach. The communication between the client part, hosted by the user browser, and the server part, hosted by a Node server, is implemented through a "web socket" channel. The communication between the client and the server is based on a service definition, including a set of operations and a data model.

![Architecture](uml/architecture.png)

- business: defines the business concepts, data and behaviors (the business data model is detailed in the following section)

- service: defines the services exchanged between the server and the client (view). Two services are proposed: the UserService specifies how the user can authenticate, the GameService supports the game lifecycle. Not only does this package specifies the services, but it also implement client-side and server-side proxies based on the WebSocket technology.

- server: implementation of a services provider on the server-side.

- views: implementation of a service consumer on the client-side. This implementation consists of rendering the current status of the session (UserService) and the game (GameService). It also processes the user actions to trigger services operations.

### Data model

The core data model of the system is summarized in the following diagram:

![Data Model](uml/abstraction.png)

The central concept of the data model is the Game which implements the dynamics of a usage session. In other words, most of the operations provided by the Game entity trigger transitions from one state of the game to another one. Other operations are related to the management of the associated data which are:

- The list of users involved in the game. The organizer, the first user that creates the game, has a specific status that provides him with special privileges. Other users are simple participants to the game.

- The list of questions to ask to the game participants. Each question is associated with a closed list of possible answers that may either be correct or incorrect. The data model does not assume that there is only one correct answer for each question.

- A volatile list of answers given by the participants for the current question. This list is used to ensure that one user does not provide several answers to one question; the score computation is therefore performed when the question is closed and the game moves to the following question (or ends).

### Services

The application is based on two different services covering two distinct part of the lifecycle: the UserService covers the establishment of the user authentication, the creation of the game or the subscription to the game. The GameService covers the lifecycle of the game, from the first question to the computation of the final score. Both services extends a common service providing connection information.

![Services](uml/services.png)

It is important to notice that the services operations are specified to support asynchronous calls. This is typically necessary to provide a WebSocket implementation but is more generally required for a dynamic web application, in order to avoid the browser main loop to be blocked by a pending synchronous call.

In terms of implementations, each service is implemented by a "Consumer" class that is used on the client side, a "Provider" class that runs on the server side.
